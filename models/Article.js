const mongoose = require('mongoose');

const { Schema } = mongoose;

const articleSchema = new Schema({
    title:  String, // String is shorthand for {type: String}
    body:   String,
    comments: [{ type: Schema.Types.ObjectId, ref: 'comment' }]
});


const articles = mongoose.model('article', articleSchema);

module.exports = articles;