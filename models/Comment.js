const mongoose = require('mongoose');

const { Schema } = mongoose;

const commentSchema = new Schema({
    commentBody:  String, // String is shorthand for {type: String}
    article: { type: Schema.Types.ObjectId, ref: 'article' },
    deletedAt : Date
});


const comments = mongoose.model('comment', commentSchema);

module.exports = comments;