const express = require('express');
const router = express.Router();
const Article = require('../models/Article');
/* GET article page. */
router.get('/', function(req, res, next) {
    Article.find()
        .populate({ path: 'comments', select: "commentBody"})
        .exec((err, docs) => {
            if(err) {
                return res.status(400).json({
                    message: "Error",
                    err
                })
            }
            res.status(200).json({
                message: "get all data",
                data: docs
            })
        })
   // Article.find((err, docs) => {
   //     res.status(200).json({
   //         message: "Get All Articles",
   //         data: docs
   //     })
   // })
});

router.get('/:id', (req, res) => {
    // doc.populated('Comment');
    Article.findOne({_id: req.params.id})
        .populate({ path: 'comments', select: "commentBody"})
        .exec((err, doc) => {
            console.log("doc", doc)
            console.log("err", err)
            if(err) {
                return res.status(400).json({
                    message: "Error",
                    err
                })
            }
            res.status(200).json({
                message: "get single data",
                data: doc
            })
        })
    // Article.findById(req.params.id, (err, docs) => {
    //     if(err) {
    //         return res.status(400).json({
    //             message: "Error",
    //             err
    //         })
    //     }
    //     res.status(200).json({
    //         message: "get new data",
    //         data: docs
    //     })
    // })
})

router.post('/', (req, res) => {
    let obj = {
        title: req.body.title,
        body: req.body.body,
    }

    let newArticle = new Article(obj)
    newArticle.save()
        .then((article) => {
            res.status(200).json({
                message: "Succesfully insert new data",
                article
            })
        })
    // Article.create({
    //     title: req.body.title,
    //     body: req.body.body,
    // }, (err) => {
    //     if(err) {
    //         return res.status(400).json({
    //             message: "Error",
    //             err
    //         })
    //     }
    //     res.status(200).json({
    //         message: "Succesfully insert new data"
    //     })
    // })
})


router.put('/:id', (req, res) => {
    Article.findByIdAndUpdate(req.params.id, { title: req.body.title, body: req.body.body }, (err, doc) => {
        if(err) {
            return res.status(400).json({
                message: "Error",
                err
            })
        }
        res.status(200).json({
            message: "Succesfully update data",
            data: doc
        })
    })
})

module.exports = router;
